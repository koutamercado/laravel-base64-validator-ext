<?php

namespace koutamercado\LaravelBase64ValidatorExt\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;

class Base64ValidatorProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Lang', 'LaravelBase64ValidatorExtLang');
        
        $this->validationExtension();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Genero una extension para las validaciones
     * @return void
     */
    private function validationExtension()
    {
        Validator::extend('base64', 'koutamercado\LaravelBase64ValidatorExt\Base64Validator@validate');
        Validator::replacer('base64', 'koutamercado\LaravelBase64ValidatorExt\Base64Validator@replace');
    }
}
