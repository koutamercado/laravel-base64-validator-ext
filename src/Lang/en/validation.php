<?php

return [
    'invalid' => 'The :attribute must be encoded in Base64'
];
