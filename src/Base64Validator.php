<?php

namespace koutamercado\LaravelBase64ValidatorExt;

class Base64Validator
{
    public function validate($attribute, $value, $parameters, $validator)
    {
        return base64_encode(base64_decode($value, true)) === $value;
    }

    public function replace($message, $attribute, $rule, $parameters)
    {
        return trans('LaravelBase64ValidatorExtLang::validation.invalid', ['attribute'=>$attribute]);
    }
}
