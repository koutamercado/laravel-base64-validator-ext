# Laravel Base64 Validator

Este paquete fue pensado para extender el facade Validator de Laravel, de esa manera poder verificar si una entrada de texto fue codificada en Base64 de lo contrario se dara una respuesta en `inglés` o `español` dependiendo la localización seteada en el framework.

## Instalación
`composer require koutamercado/laravel-base64-validator-ext`

En Laravel 5.5 o superiores, los service provider se registran automáticamente lo cual habilitara la regla `base64`.

## Ejemplo

```php
    $data = [
        'data' => base64_encode('Hola mundo'),
    ];

    $rules = [
        'data' => 'required|base64',
    ];

    $validation = Validator::make($data, $rules);

    if ($validation->fails()) {
        dd($validation->errors());
    }

    echo 'Todo bien';
```

### Respuesta en caso de error

```php
    Español: ":attribute debe estar codificado en Base64"

    Inglés: "The :attribute must be encoded in Base64"
```
